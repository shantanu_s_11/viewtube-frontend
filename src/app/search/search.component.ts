import { Component, OnInit } from '@angular/core';
import { Favourite } from '../Model/Favourite';
import { AuthService } from '../service/auth.service';
import { ViewtubeService } from '../service/viewtube.service';
import { YouTubePlayerModule } from '@angular/youtube-player';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  videos: any[];
  fav: Favourite;
  
  constructor(private _authService:AuthService,private _viewtube:ViewtubeService) { }
  search:string='';
  ngOnInit(): void {
    this.videos = [];
    this._viewtube.GetVideos().subscribe(lista=>{
      for (let element of lista["items"]) {
        this.videos.push(element);
      }
    });
  }
  searchVideo(){
    this.videos = [];
    this._viewtube.getSerchVideos(this.search).subscribe(lista=>{
      for (let element of lista["items"]) {
        this.videos.push(element);
      }
    });
  }
  AddToFavourite(id)
  {
    this.fav=new Favourite();
    localStorage.setItem('vid',id);
    var vid=localStorage.getItem('vid');
    this.fav.userid=localStorage.getItem('currentUser')
    this.fav.videoId=vid;
    this._viewtube.PutFavourites(this.fav).subscribe(data =>{},
      error=>{});
      localStorage.removeItem('vid');
  }
}
