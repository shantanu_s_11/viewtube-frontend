export class User {
    firstName?: string;
    lastName?: string;
    email?: string;
    userid?: string;
    password?: string;
}