import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LogIn } from '../Model/LogIn';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private authService:AuthService,private _router:Router) { }
  form=new FormGroup({
    uid:new FormControl('',Validators.required),
    pass:new FormControl('',Validators.required)
  });
  x:LogIn;
  message = '';
  users:any;
  ngOnInit(): void {
  }
  Login() {
    this.x=new LogIn();
    this.x.userid=this.form.get('uid').value;
    this.x.password=this.form.get('pass').value;
    localStorage.setItem('currentUser',this.x.userid); 
    this.authService.logIn(this.x).subscribe(
      data=>{
        localStorage.setItem('token', data.token);
        this._router.navigate([''])
      },
      error=>{this.message=error;alert("invalid credentials");}
    );
    this.users=this.x.userid;
  }
}
