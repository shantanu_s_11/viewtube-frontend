import { Component, OnInit } from '@angular/core';
import { YouTubePlayerModule } from '@angular/youtube-player';
import { Favourite } from '../Model/Favourite';
import { AuthService } from '../service/auth.service';
import { ViewtubeService } from '../service/viewtube.service';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {
  private _document:Document;
  favs:Array<Favourite>[];
  constructor(private _authService:AuthService,private _viewtube:ViewtubeService) { }

  ngOnInit(): void {
    var uid=localStorage.getItem('currentUser');
    this._viewtube.GetFavourites(uid).subscribe(data=>{this.favs=data;});
}
Remove(id)
{
  var uid =localStorage.getItem('currentUser');
  var vid=id;
  this._viewtube.DeleteFavourite(uid,vid).subscribe(
    data=>{},
    error=>{},
  );
  window.location.reload();
}
}
