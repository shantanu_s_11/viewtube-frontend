import { AppPage } from './app.po';
import { browser, by, element, logging } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should contain <router-outlet>', () => {
    expect(page.getRouterOutlet()).toBeTruthy();
  });
  
  it('should load register component on base url', function() {
    element(by.model('register'));});
  
  it('should load login component on base url', function() {
    element(by.model('login'));});
  
  it('should load search component on base url', function() {
    element(by.model('search'));});
  
  it('should load favourites component on base url', function() {
    element(by.model('favourites'));});
    
  it('should load home component on base url', function() {
    element(by.model('homepage'));});

  it('should load header component on base url', function() {
    element(by.model('navbar'));});

  it('should create Auth Service', function() {
    element(by.model('service'));});

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
