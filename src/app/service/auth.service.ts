import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Favourite } from '../Model/Favourite';
import { User } from '../Model/User';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loginStatus = new BehaviorSubject<boolean>(this.loggedIn());
  constructor(private http: HttpClient,private _router:Router) { }

  register(user:User): Observable<any> {
    return this.http.post<any>("http://localhost:64479/Auth/register",user);
  }

  logIn(user:User): Observable<any> {
    return this.http.post<any>("http://localhost:64479/Auth/login",user);
  }

  loggedIn(){
    return !!localStorage.getItem('token');
    
  }

  loggedOut()
  {
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    localStorage.setItem('loginStatus', '0');
    this._router.navigate(['/login']);
    alert("Logged Out Successfully");
  }
  get isLoggesIn() 
  {
      return this.loginStatus.asObservable();
  }
 
}
