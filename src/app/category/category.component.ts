import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Favourite } from '../Model/Favourite';
import { AuthService } from '../service/auth.service';
import { ViewtubeService } from '../service/viewtube.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  videos: any[];
  fav:Favourite;

  constructor(private _authService:AuthService,private spinner:NgxSpinnerService,private _viewtube:ViewtubeService) { }
categories:any[];
  ngOnInit(): void {
    this.categories = [];
    this._viewtube.getCategoryId().subscribe(lista=>{
      for (let element of lista["items"]) {
        this.categories.push(element);
      }
    });
  }
    getVideos(value){
      this.videos=[];
      var value;
      this._viewtube.getCategorySearchVideos(value).subscribe(lista=>{
        for (let element of lista["items"]) {
          this.videos.push(element);
        }
      });
    }
    addToFavourite(){
      if (this._authService.loggedIn()) {
        
      }
      else{
        alert("You need to login first");
      }
    }
AddToFavourite(id)
{
  this.fav=new Favourite();
  localStorage.setItem('vid',id);
  var vid=localStorage.getItem('vid');
  this.fav.userid=localStorage.getItem('currentUser')
  this.fav.videoId=vid;
  this._viewtube.PutFavourites(this.fav).subscribe(data =>{},
    error=>{});
    localStorage.removeItem('vid');  
}

}
