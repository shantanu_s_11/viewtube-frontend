import { TestBed } from '@angular/core/testing';

import { ViewtubeService } from './viewtube.service';

describe('ViewtubeService', () => {
  let service: ViewtubeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ViewtubeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
