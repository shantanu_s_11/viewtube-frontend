import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import { AuthService } from './auth.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('AuthService', () => {
  let service: AuthService,
  httpTestingController:HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientModule,HttpClientTestingModule,RouterTestingModule],
      providers:[AuthService]});
    service = TestBed.get(AuthService);
    httpTestingController=TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  // it('should retrieve favourites from the API',()=>{
  //   const dummy:Favourite[]=[{userid:"shantanu.shinde@cgi.com",videoId:"4JVnSkR04tM"}];
  //   service.GetFavourites("shantanu.shinde@cgi.com").subscribe(data=>{expect(data.length).toBe(1);
  //   expect(data).toEqual(dummy)}); 
  //   const request=httpTestingController.expectOne('${service.Root_Url}/shantanu.shinde@cgi.com');
  //   expect(request.request.method).toBe('Get');
  //   request.flush(dummy);
  // });

  it('should have GetFavourites method',()=>{
    expect(service.GetFavourites).toBeTruthy();
  });

  it('should have GetVideos method',()=>{
    expect(service.GetVideos).toBeTruthy();
  });

  it('should have PutFavourites method',()=>{
    expect(service.PutFavourites).toBeTruthy();
  });

  it('should have getCategoryId method',()=>{
    expect(service.getCategoryId).toBeTruthy();
  });

  it('should have getCategorySearchVideos method',()=>{
    expect(service.getCategorySearchVideos).toBeTruthy();
  });

  it('should have getSearchVideos method',()=>{
    expect(service.getSerchVideos).toBeTruthy();
  });

  it('should have login method',()=>{
    expect(service.logIn).toBeTruthy();
  });

  it('should have register method',()=>{
    expect(service.register).toBeTruthy();
  });
});
