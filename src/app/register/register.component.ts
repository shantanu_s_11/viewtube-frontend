import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../Model/User';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private _authService:AuthService,private _router:Router) { }
  form=new FormGroup({
    firstName:new FormControl('',Validators.required),
    lastName:new FormControl('',Validators.required),
    email:new FormControl('',Validators.required),
    uid:new FormControl('',Validators.required),
    pass:new FormControl('',Validators.required),
    cpass:new FormControl('',Validators.required)
  });
  message = '';
  input1 = '';
  input2 = this.input1;
  x:User;
  ngOnInit(): void {
    
  }
  getChange(event) {
    this.input2=event;
  }
  Register() {
    if (this.form.get('pass').value==this.form.get('cpass').value)
    {
      this.x=new User();
      this.x.firstName=this.form.get('firstName').value;
      this.x.lastName=this.form.get('lastName').value;
      this.x.email=this.form.get('email').value;
      this.x.userid=this.form.get('uid').value;
      this.x.password=this.form.get('pass').value;
      this._authService.register(this.x).subscribe(
        data=>{this.message=data;this._router.navigate(['']);},
        error=>{this.message=error;}
      );
    } else {
      alert("Passwords do not match! try again");
    }
    
  }
}
