import { Component } from '@angular/core';
import { AuthService } from './service/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AuthUI';
  constructor(private _authService:AuthService){}

  log():boolean
  {
    if(this._authService.loggedIn())
    return true;
  }

  logOut()
  {
    this._authService.loggedOut()
  }
}
