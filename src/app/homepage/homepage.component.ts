import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Favourite } from '../Model/Favourite';
//import { LoginComponent } from '../login/login.component';
import { User } from '../Model/User';
import { AuthService } from '../service/auth.service';
import { ViewtubeService } from '../service/viewtube.service';


@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
 
  constructor(private _authService:AuthService,private spinner: NgxSpinnerService,private _viewtube:ViewtubeService) { }
  videos: any[];
  fav:Favourite;
  refreshed = false;
  ngOnInit(): void {
    this.spinner.show()
      setTimeout(()=>
      {
      this.spinner.hide()
      },3000)
      this.videos = [];
      this._viewtube.GetVideos().subscribe(lista => {
        for (let element of lista["items"]) {
        this.videos.push(element)
        }
      });
  }
check()
{
  var tok=localStorage.getItem('token');
  if(tok!=null)
  return true;
  else
  return false;
}
add(id)
{
  this.fav=new Favourite();
  localStorage.setItem('vid',id);
  var vid=localStorage.getItem('vid');
  this.fav.userid=localStorage.getItem('currentUser')
  this.fav.videoId=vid;
  this._viewtube.PutFavourites(this.fav).subscribe(data =>{},
    error=>{});
    localStorage.removeItem('vid');
}
}
