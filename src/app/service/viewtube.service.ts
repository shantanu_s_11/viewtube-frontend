import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Favourite } from '../Model/Favourite';

@Injectable({
  providedIn: 'root'
})
export class ViewtubeService {
  private loginStatus = new BehaviorSubject<boolean>(this.loggedIn());
  constructor(private http:HttpClient,private _router:Router) { }
  GetVideos()
  {
    return this.http.get<any>("https://www.googleapis.com/youtube/v3/videos?part=snippet&chart=mostPopular&regionCode=in&maxResults=10&key=AIzaSyChGx_j4CcXh8xyWXbVazXvYSTrbxEmi48");
  }

  getCategoryId(): Observable<any> {
    return this.http.get<any>("https://www.googleapis.com/youtube/v3/videoCategories?part=snippet&regionCode=in&key=AIzaSyChGx_j4CcXh8xyWXbVazXvYSTrbxEmi48");
  }

  getCategorySearchVideos(categoryId:string): Observable<any> {
    return this.http.get<any>("https://www.googleapis.com/youtube/v3/videos?part=snippet&regionCode=in&maxResults=20&videoCategoryId="+categoryId+"&chart=mostPopular&key=AIzaSyChGx_j4CcXh8xyWXbVazXvYSTrbxEmi48");
  }
  getSerchVideos(search:string): Observable<any> {
    return this.http.get<any>("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&q="+search+"&key=AIzaSyChGx_j4CcXh8xyWXbVazXvYSTrbxEmi48");
  }

  GetFavourites(uid:string):any
  {
    return this.http.get<any>("http://localhost:49763/api/Favourite/"+uid);
  }
  
  PutFavourites(fav:Favourite)
  {
    return this.http.post<Favourite>("http://localhost:49763/api/Favourite/Add",fav);
  }

  DeleteFavourite(uid:string,vid:string)
  {
    return this.http.delete("http://localhost:49763/api/Favourite/"+uid+"/"+vid);
  }
  loggedIn(){
    return !!localStorage.getItem('token');
  }
}
