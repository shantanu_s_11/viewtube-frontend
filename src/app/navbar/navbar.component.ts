import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../service/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private _authService:AuthService) { }

  LoginStatus$ : Observable<boolean>;

  ngOnInit(): void {
    this.LoginStatus$ = this._authService.isLoggesIn;
  }
  Logout()
  {
    this._authService.loggedOut();
    window.location.reload();
  }

  show()
  {
    var id=localStorage.getItem('currentUser');
  }
}
